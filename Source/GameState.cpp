#include <States/GameState.hpp>


GameState::GameState(StateStack& stack, Context context)
: State(stack, context)
, mBoard(*context.window)
, mPlayer(*context.player)
{
}

void GameState::draw()
{
	mBoard.draw();
}

bool GameState::update(sf::Time dt)
{
	mBoard.update(dt);

	CommandQueue& commands = mBoard.getCommandQueue();
	mPlayer.handleRealtimeInput(commands);

	return true;
}

bool GameState::handleEvent(const sf::Event& event)
{
	// Game input handling
	CommandQueue& commands = mBoard.getCommandQueue();
	mPlayer.handleEvent(event, commands);

	// Escape pressed, trigger the pause screen
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
		requestStackPush(States::Pause);

	return true;
}