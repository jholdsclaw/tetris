#include <Entities/Tetromino.hpp>
#include <Resources/ResourceHolder.hpp>
#include <Utility/Utility.hpp>
#include <Utility/Foreach.hpp>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

Tetromino::Tetromino(Type type, const TextureHolder& textures)
	: mType(type)
	, mCurrentFrameIndex(0)
{

	sf::Color color = sf::Color(Tetrominos::ShapeColors[type][0],
								Tetrominos::ShapeColors[type][1],
								Tetrominos::ShapeColors[type][2],
								Tetrominos::ShapeColors[type][3]);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			mSpriteMatrix[(4 * i) + j].setTexture(textures.get(Textures::Tile));
			mSpriteMatrix[(4 * i) + j].setColor(color);
			mSpriteMatrix[(4 * i) + j].setPosition(j * 25.0f, i * 25.0f);
		}
	}

	// Populate the mFrames matrix
	std::memcpy(mFrames.data(), Tetrominos::ShapesArray[type], sizeof(Tetrominos::ShapesArray[type]));
}

void Tetromino::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (mFrames[mCurrentFrameIndex][i][j])
				target.draw(mSpriteMatrix[(4 * i) + j], states);
		}
	}
}


sf::FloatRect Tetromino::getBounds()
{
	int left = 105,
		top = 105,
		right = -1,
		bottom = -1;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (mFrames[mCurrentFrameIndex][i][j])
			{
				if (mSpriteMatrix[(4 * i) + j].getPosition().x < left)
					left = mSpriteMatrix[(4 * i) + j].getPosition().x;
				if (mSpriteMatrix[(4 * i) + j].getPosition().x > right)
					right = mSpriteMatrix[(4 * i) + j].getPosition().x;
				if (mSpriteMatrix[(4 * i) + j].getPosition().y < top)
					top = mSpriteMatrix[(4 * i) + j].getPosition().y;
				if (mSpriteMatrix[(4 * i) + j].getPosition().y > bottom)
					bottom = mSpriteMatrix[(4 * i) + j].getPosition().y;
			}
		}
	}

	return sf::FloatRect(left, top, (right - left + 25.0f), (bottom - top + 25.0f));
}

void Tetromino::rotateClockWise()
{
	mCurrentFrameIndex++;
	if (mCurrentFrameIndex >= 4)
		mCurrentFrameIndex = 0;
}

// TODO: Shouldn't this be somehow refactored into the input definitions?
unsigned int Tetromino::getCategory() const
{
	return Category::ActiveTetromino;
}
