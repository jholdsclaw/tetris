#include <World/Board.hpp>

#include <SFML/Graphics/RenderWindow.hpp>

#include <algorithm>
#include <cmath>


Board::Board(sf::RenderWindow& window)
: mWindow(window)
, mBoardView(window.getDefaultView())
, mTextures() 
, mSceneGraph()
, mSceneLayers()
, mBoardBounds(0.f, 0.f, mBoardView.getSize().x, mBoardView.getSize().y)
, mSpawnPosition(mBoardView.getSize().x / 2.f, mBoardBounds.height - mBoardView.getSize().y / 2.f - (25.f * 10))
, mActiveTetromino(nullptr)
{

	// TODO: implement the loading state (ParallelTask & LoadingState)

	loadTextures();
	buildScene();

	// Prepare the view
	mBoardView.setCenter(mBoardView.getSize().x / 2.f, mBoardBounds.height - mBoardView.getSize().y / 2.f);


}

void Board::update(sf::Time dt)
{
	// Forward commands to scene graph, adapt velocity (scrolling, diagonal correction)
	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);

	// Regular update step, adapt position (correct if outside view)
	mSceneGraph.update(dt);
	adaptPlayerPosition();
}

void Board::draw()
{
	mWindow.setView(mBoardView);
	mWindow.draw(mSceneGraph);
}

CommandQueue& Board::getCommandQueue()
{
	return mCommandQueue;
}

void Board::loadTextures()
{
	mTextures.load(Textures::BoardBackground, texturePath() + "Background.png");
	mTextures.load(Textures::Tile, texturePath() + "Tile.png");
}

void Board::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < LayerCount; ++i)
	{
		SceneNode::Ptr layer(new SceneNode());
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	// Prepare the background
	sf::Texture& texture = mTextures.get(Textures::BoardBackground);
	sf::IntRect textureRect(mBoardBounds);

	// Add the background sprite to the scene
	std::unique_ptr<SpriteNode> backgroundSprite(new SpriteNode(texture, textureRect));
	backgroundSprite->setPosition(mBoardBounds.left, mBoardBounds.top);
	mSceneLayers[Background]->attachChild(std::move(backgroundSprite));

	// Add player's avatar
	std::unique_ptr<Tetromino> leader(new Tetromino(Tetromino::I, mTextures));
	mActiveTetromino = leader.get();
	mActiveTetromino->setPosition(mSpawnPosition);
	mSceneLayers[Air]->attachChild(std::move(leader));

}

void Board::adaptPlayerPosition()
{
	// Keep player's position inside the board bounds
	sf::FloatRect gameBounds(mBoardView.getCenter() - (sf::Vector2f(5, 10) * 25.0f), (sf::Vector2f(10.0f, 20.0f) * 25.0f));

	sf::Vector2f position = mActiveTetromino->getPosition();
	sf::FloatRect bounds = mActiveTetromino->getBounds();

	position.x = std::max(position.x, gameBounds.left -  bounds.left);
	position.x = std::min(position.x, gameBounds.left + gameBounds.width - (bounds.left + bounds.width));
	position.y = std::max(position.y, gameBounds.top - bounds.top);
	position.y = std::min(position.y, gameBounds.top + gameBounds.height - (bounds.top + bounds.height));
	mActiveTetromino->setPosition(position);
}
