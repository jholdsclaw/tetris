#include <Input/Player.hpp>
#include <Input/CommandQueue.hpp>
#include <Entities/Tetromino.hpp>
#include <Utility/Foreach.hpp>

#include <map>
#include <string>
#include <algorithm>


struct TetrominoMover
{
	TetrominoMover(Player::Action action)
	: action(action)
	{
	}

	void operator() (Tetromino& tetromino, sf::Time) const
	{
		switch (action)
		{
		case Player::MoveLeft:
			tetromino.move(-25, 0);
			break;
		case Player::MoveRight:
			tetromino.move(25, 0);
			break;
		case Player::MoveDown:
			tetromino.move(0, 25);
			break;
		case Player::RotateClockWise:
			tetromino.rotateClockWise();
		default:
			return;
		}
	}

	Player::Action action;
};

Player::Player()
{
	// Set initial key bindings
	mKeyBinding[sf::Keyboard::Left] = MoveLeft;
	mKeyBinding[sf::Keyboard::Right] = MoveRight;
	mKeyBinding[sf::Keyboard::Down] = MoveDown;
	mKeyBinding[sf::Keyboard::Up] = RotateClockWise;

	// Set initial action bindings
	initializeActions();

	// Assign all categories to player's aircraft
	FOREACH(auto& pair, mActionBinding)
		pair.second.category = Category::ActiveTetromino;
}

void Player::handleEvent(const sf::Event& event, CommandQueue& commands)
{
	if (event.type == sf::Event::KeyPressed)
	{
		// Check if pressed key appears in key binding, trigger command if so
		auto found = mKeyBinding.find(event.key.code);
		if (found != mKeyBinding.end() && !isRealtimeAction(found->second))
			commands.push(mActionBinding[found->second]);
	}
}

void Player::handleRealtimeInput(CommandQueue& commands)
{
	// Traverse all assigned keys and check if they are pressed
	FOREACH(auto pair, mKeyBinding)
	{
		// If key is pressed, lookup action and trigger corresponding command
		if (sf::Keyboard::isKeyPressed(pair.first) && isRealtimeAction(pair.second))
			commands.push(mActionBinding[pair.second]);
	}
}

void Player::assignKey(Action action, sf::Keyboard::Key key)
{
	// Remove all keys that already map to action
	for (auto itr = mKeyBinding.begin(); itr != mKeyBinding.end();)
	{
		if (itr->second == action)
			mKeyBinding.erase(itr++);
		else
			++itr;
	}

	// Insert new binding
	mKeyBinding[key] = action;
}

sf::Keyboard::Key Player::getAssignedKey(Action action) const
{
	FOREACH(auto pair, mKeyBinding)
	{
		if (pair.second == action)
			return pair.first;
	}

	return sf::Keyboard::Unknown;
}

void Player::initializeActions()
{
	const float playerSpeed = 200.f;

	mActionBinding[MoveLeft].action = derivedAction<Tetromino>(TetrominoMover(MoveLeft));
	mActionBinding[MoveRight].action = derivedAction<Tetromino>(TetrominoMover(MoveRight));
	mActionBinding[MoveDown].action = derivedAction<Tetromino>(TetrominoMover(MoveDown));
	mActionBinding[RotateClockWise].action = derivedAction<Tetromino>(TetrominoMover(RotateClockWise));
}

bool Player::isRealtimeAction(Action action)
{
	switch (action)
	{
// TODO
//		return true;

	case MoveLeft:
	case MoveRight:
	case MoveDown:
	case RotateClockWise:
		return false;
	default:
		return false;
	}
}
