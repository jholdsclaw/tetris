#pragma once

#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

#include <States/State.hpp>
#include <World/Board.hpp>
#include <Input/Player.hpp>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class GameState : public State
{
	public:
							GameState(StateStack& stack, Context context);

		virtual void		draw();
		virtual bool		update(sf::Time dt);
		virtual bool		handleEvent(const sf::Event& event);


	private:
		Board				mBoard;
		Player&				mPlayer;
};

#endif // GAMESTATE_HPP