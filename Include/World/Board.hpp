#pragma once

#ifndef BOARD_HPP
#define BOARD_HPP

#include <Resources/ResourceHolder.hpp>
#include <Resources/ResourceIdentifiers.hpp>
#include <Resources/SceneNode.hpp>
#include <Resources/SpriteNode.hpp>
#include <Entities/Tetromino.hpp>
#include <Input/CommandQueue.hpp>
#include <Input/Command.hpp>

#include <Utility/ResourcePath.hpp>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <array>


// Forward declaration
namespace sf
{
	class RenderWindow;
}

class Board : private sf::NonCopyable
{
	public:

		struct Tile
		{
			public:
				Tile() : active(false) { };

				bool		active;
				sf::Color	color;
		};

	public:
		explicit							Board(sf::RenderWindow& window);
		void								update(sf::Time dt);
		void								draw();

		CommandQueue&						getCommandQueue();


	private:
		void								loadTextures();
		void								buildScene();
		void								adaptPlayerPosition();


	private:
		enum Layer
		{
			Background,
			Air,
			LayerCount
		};


	private:
		sf::RenderWindow&					mWindow;
		sf::View							mBoardView;
		TextureHolder						mTextures;

		SceneNode							mSceneGraph;
		std::array<SceneNode*, LayerCount>	mSceneLayers;
		CommandQueue						mCommandQueue;

		sf::FloatRect						mBoardBounds;
		sf::Vector2f						mSpawnPosition;

		Tetromino*							mActiveTetromino;
		std::array<std::array<Tile, 20>, 10>	mGameBoard;
};

#endif // BOARD_HPP
