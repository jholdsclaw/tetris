#pragma once

#ifndef TETROMINO_HPP
#define TETROMINO_HPP

#include <Entities/Entity.hpp>
#include <Resources/ResourceIdentifiers.hpp>

#include <SFML/Graphics/Sprite.hpp>

#include <array>

class Tetromino : public Entity
{
public:
	enum Type
	{
		I,
		J,
		L,
		O,
		S,
		T,
		Z,
		TetrominoCount
	};

	Tetromino(Type type, const TextureHolder& textures);

	virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual unsigned int	getCategory() const;

	// TODO: Refactor all these to ints
	sf::FloatRect			getBounds();

	void					rotateClockWise();

private:
	Type					mType;
	int						mCurrentFrameIndex;

	std::array<sf::Sprite, 16>							mSpriteMatrix;
	std::array<std::array<std::array<bool, 4>, 4>, 4>	mFrames;

};

#include <Entities/Tetrominos.inl>

#endif // TETROMINO_HPP
